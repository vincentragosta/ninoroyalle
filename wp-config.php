<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         ')=YAX2-d=G?bFpRhW*Y2@nYB@kyu~H&7]$inC:9#IV+dEA%*5ROB)S{8Fp.RAhZ&');
define('SECURE_AUTH_KEY',  '{?pya:wKl:sUF_H#=1_TdpR&pHe-5btc,4.T(O,/]s%EL6z|RsPYI5+w$s-^CR1-');
define('LOGGED_IN_KEY',    '%kQ.0K%6du{c?rF|I~yxOfq*=-mW:3JHH =clA?6X-DUU&1%b-|%!a|&AwI|(yq}');
define('NONCE_KEY',        ')QZKm$_}S#ncZ0ert?a|n,kz+Kjq{[58i#Ohsk|c?hvoSxs4y{nG-+Ja|G#Mdu4(');
define('AUTH_SALT',        'JygbnHHvy4d.bw<sf9Mt:?$wW,q((--{-[_6VB 2F)X4.OLeM7!g*~<rZ+E]Q(=|');
define('SECURE_AUTH_SALT', 'g5U*_/wh{.K$&_zFjV2H+fWeq,l&1OM<@{ fy$`xHM17+%y=)yarP.K[Q3]_aE3,');
define('LOGGED_IN_SALT',   '|Y+/++-vMb]Lak9@876G*P0lZJnRV-D>+nd2N@xx:Oyo|{!4xiE(a@@pE-G~+f[^');
define('NONCE_SALT',       'G2=u=)}!(kr;R#2M`NOr:[DnAb9A-ufQ!/~^ij~IHqzNH7r<E.w8>Gw9qDeoR<fT');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
