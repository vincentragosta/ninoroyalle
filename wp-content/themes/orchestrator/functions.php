<?php

use Orchestrator\Theme;

global $Theme;

if (empty($Theme)) {
    $Theme = new Theme();
}
