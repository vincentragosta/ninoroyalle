<?php

use ChildTheme\Components\PostActionCard\PostActionCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Recipe\Recipe;

global $wp_query, $post;
$RecipePage = GlobalOptions::recipePage();
?>

<?php if (!empty($content = $RecipePage->content(false))): ?>
    <?= $content; ?>
<?php endif; ?>
<?php if ($wp_query->have_posts()): ?>
    <section class="content-section content-section--mb-double">
        <div class="container content-section__container">
            <div class="content-row row">
                <?php while($wp_query->have_posts()): the_post(); ?>
                    <div class="content-column col-md-12 content-column--last">
                        <div class="content-column__inner">
                            <?= new PostActionCardView(new Recipe($post)); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <?php if ($wp_query->found_posts > $wp_query->query_vars['posts_per_page']): ?>
        <section class="content-section content-section--mb-double">
            <div class="content-section__container container">
                <div class="content-row row">
                    <div class="content-column text--center col-md-12">
                        <div class="content-column__inner">
                            <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php endif; ?>
