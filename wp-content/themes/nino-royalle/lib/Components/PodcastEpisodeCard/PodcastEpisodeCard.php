<?php

namespace ChildTheme\Components\PodcastEpisodeCard;

use Backstage\Models\PostBase;
use Backstage\VcLibrary\Support\Component;
use ChildTheme\Podcast\PodcastEpisode;
use ChildTheme\Podcast\PodcastEpisodeRepository;

/**
 * Class PodcastEpisodeCard
 * @package ChildTheme\Components\PodcastEpisodeCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PodcastEpisodeCard extends Component
{
    const NAME = 'Podcast Episode Card';
    const TAG = 'podcast_episode_card';
    const VIEW = PodcastEpisodeCardView::class;

    protected $component_config = [
        'description' => 'Create a podcast episode card.',
        'icon' => 'icon-wpb-atm',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'dropdown',
                'heading' => 'Post',
                'param_name' => 'podcast_episode_id',
                'value' => '',
                'description' => 'Select a podcast episode from the dropdown.',
                'admin_label' => true
            ]
        ]
    ];

    public function setupConfig()
    {
        $options['-- Select Post --'] = '';
        $PodcastEpisodeRepository = new PodcastEpisodeRepository();
        $podcast_episodes = $PodcastEpisodeRepository->findAll();
        foreach($podcast_episodes as $Post) {
            /* @var PostBase $Post */
            $options[$Post->post()->post_title] = $Post->ID;
        }
        $this->component_config['params'][0]['value'] = $options;
        parent::setupConfig();
    }

    protected function createView(array $atts)
    {
        /* @var PodcastEpisodeCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(new PodcastEpisode($atts['podcast_episode_id']));
    }
}
