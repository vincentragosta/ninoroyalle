<?php

namespace ChildTheme\Components\PodcastEpisodeCard;

use Backstage\View\Component;
use Backstage\View\Link;
use ChildTheme\Podcast\PodcastEpisode;

/**
 * Class PodcastEpisodeCardView
 * @package ChildTheme\Components\PodcastEpisodeCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property \WP_Image|bool $image
 * @property string $title
 * @property string $url
 * @property string $source
 * @property string $content
 * @property array $sharer_icons
 */
class PodcastEpisodeCardView extends Component
{
    const IMAGE_SIZE = 768;
    const NUM_WORDS = 35;

    protected $name = 'podcast-episode-card';
    protected static $default_properties = [
        'image' => false,
        'title' => '',
        'url' => '',
        'source' => '',
        'content' => '',
        'sharer_icons' => []
    ];

    public function __construct(PodcastEpisode $PodcastEpisode)
    {
        parent::__construct([
            'image' => $PodcastEpisode->featuredImage(),
            'title' => $PodcastEpisode->title(),
            'source' => $PodcastEpisode->source,
            'url' => $PodcastEpisode->url,
            'content' => $PodcastEpisode->content(false),
            'sharer_icons' => $PodcastEpisode->sharer_icons
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_SIZE)->height(static::IMAGE_SIZE);
        }
        if (!empty($url = $this->url)) {
            $this->image = new Link($url, $this->image, ['target' => '_blank']);
            $this->title = new Link($url, $this->title, ['target' => '_blank']);
        }
        if ($this->content) {
            $this->content = wp_trim_words($this->content, static::NUM_WORDS);
        }
    }
}
