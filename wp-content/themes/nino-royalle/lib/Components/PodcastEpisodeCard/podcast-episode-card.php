<?php
/**
 * Expected:
 * @var \WP_Image $image
 * @var string $title
 * @var string $source
 * @var array $sharer_icons
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\SocialIcons\SocialIconsView;
use Backstage\Util;

if (empty($title) || empty($source)) {
    return '';
}

$element_attributes['class'] = 'responsive-component';
?>

<div <?= Util::componentAttributes('podcast-episode', $class_modifiers, $element_attributes); ?>>
    <div class="podcast-episode__image">
        <?= $image; ?>
        <div class="podcast-episode__overlay">
            <p class="podcast-episode__overlay-title"><?= $title; ?></p>
        </div>
    </div>
    <div class="podcast-episode__content">
        <h3 class="podcast-episode__heading heading"><?= $title; ?></h3>
        <?php if ($content): ?>
            <p class="podcast-episode__description"><?= $content; ?></p>
        <?php endif; ?>
        <?php if (!empty($sharer_icons)): ?>
            <div class="podcast-episode__sharer-icons">
                <?= SocialIconsView::create($sharer_icons); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
