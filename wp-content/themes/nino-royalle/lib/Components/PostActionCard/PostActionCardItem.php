<?php

namespace ChildTheme\Components\PostActionCard;

/**
 * Interface PostActionCardItem
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
interface PostActionCardItem
{
    public function getTagline();
}
