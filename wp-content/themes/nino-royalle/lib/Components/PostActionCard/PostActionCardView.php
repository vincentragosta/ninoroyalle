<?php

namespace ChildTheme\Components\PostActionCard;

use Backstage\Models\PostBase;
use Backstage\Support\DateTime;
use Backstage\View\Component;
use Backstage\View\Link;
use ChildTheme\SocialSharer\SocialSharerLinks;

/**
 * Class PostActionCardView
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $content
 * @property string $image
 * @property string $url
 * @property array $social_sharer
 * @property string $date_comments
 * @property array $class_modifiers
 * @property array $element_attributes
 */
class PostActionCardView extends Component
{
    const TRIM_WORD_COUNT = 30;
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 576;
    const DATE_FORMAT = 'd F, Y';
    const COMMENTS_LABEL = '%s comments';
    const DATE_COMMENTS_LABEL = '%s / %s';

    protected $name = 'post-action-card';
    protected static $default_properties = [
        'title' => '',
        'tagline' => '',
        'content' => '',
        'image' => false,
        'url' => '',
        'social_sharer' => [],
        'comments_number' => 0
    ];

    public function __construct(PostActionCardItem $Post)
    {
        $args = [
            'url' => $Post->permalink(),
            'title' => $Post->title(),
            'tagline' => $Post->getTagline(),
            'content' => $Post->content(false),
            'image' => $Post->featuredImage(),
            'date_comments' => sprintf(
                static::DATE_COMMENTS_LABEL,
                $Post->publishedDate()->format(static::DATE_FORMAT),
                new Link($Post->permalink(), sprintf(static::COMMENTS_LABEL, $Post->getCommentsNumber()))
            ),
        ];
        if ($Post instanceof SocialSharerLinks) {
            $args['social_sharer'] = $Post->getSocialSharerLinks();
        }
        parent::__construct($args);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
        if ($this->content) {
            $this->content = wp_trim_words($this->content, static::TRIM_WORD_COUNT, '&hellip;');
        }
    }
}
