<?php

namespace ChildTheme\BlogPost;

use ChildTheme\Components\PostActionCard\PostActionCardItem;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\PinterestSocialSharer;
use ChildTheme\SocialSharer\SocialSharerLinks;
use ChildTheme\SocialSharer\TwitterSocialSharer;
use ChildTheme\RelatedContent\RelatedContentPost;

/**
 * Class BlogPost
 * @package ChildTheme\BlogPost
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class BlogPost extends RelatedContentPost implements SocialSharerLinks, PostActionCardItem
{
    const TAXONOMY = 'category';
    const RELATED_TAXONOMY = 'category';
    const POST_TYPE = 'post';

    public function getSocialSharerLinks(): array
    {
        $url = $this->permalink();
        return [
            new FacebookSocialSharer($url),
            new TwitterSocialSharer($url, $this->title()),
            new PinterestSocialSharer($url, $this->excerpt() ?: $this->title())
        ];
    }

    public function getTagline()
    {
        return '';
    }

    public function getCommentsNumber()
    {
        return get_comments_number($this->ID);
    }
}
