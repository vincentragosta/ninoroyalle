<?php

namespace ChildTheme\BlogPost;

/**
 * Class BlogPostController
 * @package ChildTheme\BlogPost
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class BlogPostController
{
    const CONTEXT = 'normal';

    public function __construct()
    {
        add_action('admin_menu', [$this, 'removeDefaultMetaboxes']);
        add_action('edit_form_after_title', [$this, 'addEditorToBlogPage']);
    }

    public function removeDefaultMetaboxes()
    {
        remove_meta_box('categorydiv', BlogPost::POST_TYPE, static::CONTEXT);
        remove_meta_box('tagsdiv-post_tag', BlogPost::POST_TYPE, static::CONTEXT);
    }

    public function addEditorToBlogPage(\WP_Post $Post)
    {
        if ($Post->ID !== get_option('page_for_posts')) {
            return;
        }
        remove_action('edit_form_after_title', '_wp_posts_page_notice');
        add_post_type_support('page', 'editor');
    }
}
