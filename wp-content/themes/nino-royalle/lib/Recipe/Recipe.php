<?php

namespace ChildTheme\Recipe;

use ChildTheme\Components\PostActionCard\PostActionCardItem;
use ChildTheme\RelatedContent\RelatedContentPost;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\PinterestSocialSharer;
use ChildTheme\SocialSharer\SocialSharerLinks;
use ChildTheme\SocialSharer\TwitterSocialSharer;

/**
 * Class Recipe
 * @package ChildTheme\Recipe
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $prep_time
 * @property string $cook_time
 * @property string serving_size
 * @property array $ingredients
 * @property array $directions
 */
class Recipe extends RelatedContentPost implements SocialSharerLinks, PostActionCardItem
{
    const POST_TYPE = 'recipe';
    const RELATED_TAXONOMY = 'recipe-type';

    public function getSocialSharerLinks(): array
    {
        $url = $this->permalink();
        return [
            new FacebookSocialSharer($url),
            new TwitterSocialSharer($url, $this->title()),
            new PinterestSocialSharer($url, $this->excerpt() ?: $this->title())
        ];
    }

    public function getTagline()
    {
        $parts = [];
        if (!empty($prep_time = $this->prep_time)) {
            $parts[] = sprintf('<strong>Prep Time:</strong> %d mins', $prep_time);
        }
        if (!empty($cook_time = $this->cook_time)) {
            $parts[] = sprintf('<strong>Cook Time:</strong> %d mins', $cook_time);
        }
        if (!empty($serving_size = $this->serving_size)) {
            $parts[] = sprintf('<strong>Serving Size:</strong> %s', $serving_size);
        }
        return implode(' | ', $parts);
    }

    public function getCommentsNumber()
    {
        return get_comments_number($this->ID);
    }
}
