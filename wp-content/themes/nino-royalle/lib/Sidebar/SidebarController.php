<?php

namespace ChildTheme\Sidebar;

/**
 * Class SidebarController
 * @package ChildTheme\Sidebar
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class SidebarController
{
    const SIDEBAR_SETTINGS = [
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="content-column content-column--widget col-md-12"><div class="content-column__inner">',
        'after_widget' => '</div></div>',
        'before_title' => '<h2 class="heading heading--large">',
        'after_title' => '</h2>'
    ];

    const WIDGETS_BLACKLIST = [
        'WP_Widget_Pages',
        'WP_Widget_Calendar',
        'WP_Widget_Archives',
        'WP_Widget_Links',
        'WP_Widget_Meta',
        'WP_Widget_Search',
        'WP_Widget_Text',
        'WP_Widget_Recent_Comments',
        'WP_Widget_RSS',
        'WP_Widget_Tag_Cloud',
        'WP_Widget_Media_Audio',
        'WP_Widget_Media_Gallery',
        'WP_Widget_Media_Video',
        'WP_Widget_Media_Image',
        'WP_Widget_Custom_HTML',
        'WP_Nav_Menu_Widget',
        'Twenty_Eleven_Ephemera_Widget'
    ];

    public function __construct()
    {
        add_action('widgets_init', [$this, 'addSidebar']);
        add_action('widgets_init', [$this, 'removeWidgets']);
        add_filter('widget_posts_args', [$this, 'updateRecentPostsWidgetArgs']);
    }

    public function addSidebar()
    {
        register_sidebar(static::SIDEBAR_SETTINGS);
    }

    public function removeWidgets()
    {
        if (empty($widgets = static::WIDGETS_BLACKLIST)) {
            return;
        }
        return array_walk($widgets, 'unregister_widget');
    }

    public function updateRecentPostsWidgetArgs(array $args)
    {
        return array_merge($args, [
            'before_widget' => '<div class'
        ]);
    }
}
