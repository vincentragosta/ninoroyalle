<?php

namespace ChildTheme\Sidebar\Widgets;

use ChildTheme\Components\PodcastEpisodeCard\PodcastEpisodeCardView;
use ChildTheme\Podcast\PodcastEpisode;
use ChildTheme\Podcast\PodcastEpisodeRepository;

/**
 * Class RecentPodcast
 * @package ChildTheme\Sidebar\Widgets
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RecentPodcast extends \WP_Widget
{
    const OPTIONS = [
        'classname' => 'widget-recent-podcast',
        'description' => 'Display the most recent podcast.'
    ];

    public function __construct()
    {
        parent::__construct('recent-podcast', __('Recent Podcast'), static::OPTIONS);
    }

    public function register()
    {
        add_action('widgets_init', function () {
            register_widget(static::class);
        });
    }

    public function widget($args, $instance)
    {
        $PodcastEpisodeRepository = new PodcastEpisodeRepository();
        $PodcastEpisode = $PodcastEpisodeRepository->findOne();
        if (!$PodcastEpisode instanceof PodcastEpisode) {
            return;
        }
        $title = (!empty($instance['title'])) ? $instance['title'] : __('Recent Posts');
        echo $args['before_widget'];
        if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo (new PodcastEpisodeCardView($PodcastEpisode))->classModifiers('no-sharer-icons');
        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['video_id'] = sanitize_text_field($new_instance['video_id']);
        return $instance;
    }

    public function form($instance)
    {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>
        <p><label for="<?= $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?= $this->get_field_id('title'); ?>"
                   name="<?= $this->get_field_name('title'); ?>" type="text" value="<?= $title; ?>"/></p>
        <?php
    }
}
