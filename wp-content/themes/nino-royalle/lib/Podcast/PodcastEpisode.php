<?php

namespace ChildTheme\Podcast;

use Backstage\Models\PostBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;

/**
 * Class PodcastEpisode
 * @package ChildTheme\Podcast
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $source
 * @property string $url
 * @property array $sharer_icons
 */
class PodcastEpisode extends PostBase
{
    const POST_TYPE = 'podcast-episode';
    const TAXONOMY = 'podcast';

    public function getSharerIcons()
    {
        if (empty($sharer_icons = $this->field('sharer_icons'))) {
            return [];
        }
        return array_map(function($icon) {
            return SocialIcon::create($icon['icon'], $icon['url']);
        }, $sharer_icons);
    }
}
