<?php

namespace ChildTheme\Podcast;

use Backstage\Repositories\PostRepository;

/**
 * Class PodcastEpisodeRepository
 * @package ChildTheme\Podcast
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PodcastEpisodeRepository extends PostRepository
{
    protected $model_class = PodcastEpisode::class;

    public function findOne(array $query = [])
    {
        $query['posts_per_page'] = 1;
        $posts = $this->find($query);
        return $posts[0] ?? false;
    }
}
