<?php

namespace ChildTheme\Podcast;

/**
 * Class PodcastEpisodeController
 * @package ChildTheme\Podcast
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PodcastEpisodeController
{
    public function __construct()
    {
        add_action('pre_get_posts', [$this, 'mainQuery']);
    }

    public function mainQuery(\WP_Query $query)
    {
        if ($query->is_post_type_archive(PodcastEpisode::POST_TYPE) && $query->is_main_query() && !$query->is_paged()) {
            $query->set('posts_per_page', get_option('posts_per_page') + 1);
        }
    }
}
