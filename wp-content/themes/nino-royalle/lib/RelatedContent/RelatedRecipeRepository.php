<?php

namespace ChildTheme\RelatedContent;

use Backstage\Producers\RelatedContent\RelatedContentRepository;
use ChildTheme\BlogPost\BlogPost;
use ChildTheme\Recipe\Recipe;

/**
 * Class RelatedRecipeRepository
 * @package ChildTheme\RelatedContent
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RelatedRecipeRepository extends RelatedContentRepository
{
    protected $model_class = Recipe::class;
    protected $related_model_class = BlogPost::class;
}
