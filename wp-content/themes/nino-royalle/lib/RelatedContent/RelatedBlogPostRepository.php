<?php

namespace ChildTheme\RelatedContent;

use Backstage\Producers\RelatedContent\RelatedContentRepository;
use ChildTheme\BlogPost\BlogPost;
use ChildTheme\Recipe\Recipe;

/**
 * Class RelatedBlogPostRepository
 * @package ChildTheme\RelatedContent
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class RelatedBlogPostRepository extends RelatedContentRepository
{
    protected $model_class = BlogPost::class;
    protected $related_model_class = Recipe::class;
}
