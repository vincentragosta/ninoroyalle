<?php

namespace ChildTheme;

use ChildTheme\BlogPost\BlogPostController;
use ChildTheme\Podcast\PodcastEpisodeController;
use ChildTheme\Search\SearchController;
use ChildTheme\Sidebar\Widgets;
use ChildTheme\Sidebar\SidebarController;
use ChildTheme\Support\NavMenuHandler;
use ChildTheme\Components;
use ChildTheme\Support\PreferredDimensionsHandler;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const REMOVE_COMMENTS = false;
    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'video-producer',
        'preview-producer'
    ];

    const EXTENSIONS = [
        NavMenuHandler::class,
        PreferredDimensionsHandler::class,
        SidebarController::class,
        SearchController::class,
        PodcastEpisodeController::class,
        BlogPostController::class,
        Widgets\RecentPodcast::class,
        Widgets\Video::class,
        Components\PodcastEpisodeCard\PodcastEpisodeCard::class,
        Components\PostActionCard\PostActionCard::class
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_style('mediaelement');
        wp_enqueue_script('mediaelement');
        wp_enqueue_script('masonryjs', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', ['jquery'], '1.0', true);
    }
}
