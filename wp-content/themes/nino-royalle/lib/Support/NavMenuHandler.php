<?php

namespace ChildTheme\Support;

use Backstage\SetDesign\Icon\IconView;

/**
 * Class NavMenuHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class NavMenuHandler
{
    public function __construct()
    {
        add_filter('nav_menu_css_class', [$this, 'addIconToDropdown'], 10, 2);
    }

    public function addIconToDropdown($classes, $item)
    {
        $classes_map = array_combine($classes, $classes);
        if (isset($classes_map['menu-item-has-children'])) {
            $item->title .= new IconView(['icon_name' => 'arrow']);
        }
        return $classes;
    }
}
