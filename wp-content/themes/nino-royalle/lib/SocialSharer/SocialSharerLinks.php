<?php

namespace ChildTheme\SocialSharer;

/**
 * Interface SocialSharerLinks
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
interface SocialSharerLinks
{
    function getSocialSharerLinks(): array;
}
