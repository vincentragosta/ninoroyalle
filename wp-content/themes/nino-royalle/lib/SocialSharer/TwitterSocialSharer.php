<?php

namespace ChildTheme\SocialSharer;

/**
 * Class TwitterSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TwitterSocialSharer extends SocialSharer
{
    const SHARER = 'https://twitter.com/intent/tweet?original_referer=%s&source=tweetbutton&text=%s';
    const ICON = 'twitter';

    protected function generateSharerUrl(string $url, string $text = null)
    {
        if (empty($url)) {
            return '';
        }
        return sprintf(static::SHARER, $url, $text);
    }
}
