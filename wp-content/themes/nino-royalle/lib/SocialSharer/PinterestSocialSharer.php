<?php

namespace ChildTheme\SocialSharer;

/**
 * Class PinterestSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PinterestSocialSharer extends SocialSharer
{
    const SHARER = 'http://pinterest.com/pin/create/button/?url=%s&description=%s';
    const ICON = 'pinterest';

    protected function generateSharerUrl(string $url, string $text = null)
    {
        if (empty($url)) {
            return '';
        }
        return sprintf(static::SHARER, $url, $text);
    }
}
