<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\Models\Page;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Podcast\PodcastEpisode;

/**
 * Class GlobalOptions
 * @package ChildTheme\Options\
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @method static featuredPodcastText()
 * @method static featuredPodcastEpisode()
 * @method static podcastPage()
 * @method static recipePage()
 * @method static socialIcons()
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'featured_podcast_text' => '',
        'featured_podcast_episode' => '',
        'social_icons' => [],
        'podcast_page' => null,
        'recipe_page' => null
    ];

    protected function getFeaturedPodcastEpisode()
    {
        $podcast_episode = $this->get('featured_podcast_episode');
        if (empty($podcast_episode)) {
            return null;
        }
        return new PodcastEpisode($podcast_episode);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }

    protected function getRecipePage()
    {
        return new Page($this->get('recipe_page'));
    }

    protected function getPodcastPage()
    {
        return new Page($this->get('podcast_page'));
    }
}
