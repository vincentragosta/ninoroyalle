<?php

use ChildTheme\Components\PodcastEpisodeCard\PodcastEpisodeCardView;
use ChildTheme\Options\GlobalOptions;
use ChildTheme\Podcast\PodcastEpisode;

global $wp_query, $post;
$PodcastPage = GlobalOptions::podcastPage();
?>

<?php if (!empty($content = $PodcastPage->content(false))): ?>
    <?= $content; ?>
<?php endif; ?>
<?php if ($wp_query->have_posts()): ?>
    <section class="content-section content-section--mb-double">
        <div class="container content-section__container">
            <div class="content-row row">
                <?php while($wp_query->have_posts()): the_post(); ?>
                    <?php if ($wp_query->current_post == 0 && !$wp_query->is_paged): ?>
                        <div class="content-column col-md-12 content-column--last">
                            <div class="content-column__inner">
                                <?= new PodcastEpisodeCardView(new PodcastEpisode($post)); ?>
                            </div>
                        </div></div><div class="content-row row">
                    <?php else: ?>
                        <div class="content-column col-md-3 content-column--last">
                            <div class="content-column__inner">
                                <?= new PodcastEpisodeCardView(new PodcastEpisode($post)); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <?php if ($wp_query->found_posts > $wp_query->query_vars['posts_per_page']): ?>
        <section class="content-section content-section--mb-double">
            <div class="content-section__container container">
                <div class="content-row row">
                    <div class="content-column text--center col-md-12">
                        <div class="content-column__inner">
                            <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
<?php endif; ?>
