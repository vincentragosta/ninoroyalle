addAction(INIT, function() {
    var EXPANSE_BREAKPOINT = 768;
    var EXPANSE_CLASS = 'responsive-component--expanse';
    var $components = $('.responsive-component');

    if (!$components.length) {
        return;
    }

    function toggleComponentClasses(components) {
        components.each(function (key, component) {
            var $component = $(component);
            if ($component.width() === 0) {
                return;
            }
            $component.toggleClass(EXPANSE_CLASS, $component.width() >= EXPANSE_BREAKPOINT);
        });
    }

    toggleComponentClasses($components);
    addAction(LAYOUT, function() {
        toggleComponentClasses($components);
    });
});
