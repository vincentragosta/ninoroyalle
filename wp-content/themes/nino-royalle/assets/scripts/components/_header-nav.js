addAction(INIT, function() {
    var $searchButton = $('.header-nav__search-button');
    $searchButton.click(function() {
        var $searchDrawer = $('.header-nav__search-drawer');
        $searchDrawer.toggleClass('header-nav__search-drawer--open', !$searchDrawer.hasClass('header-nav__search-drawer--open'));
    });
});
