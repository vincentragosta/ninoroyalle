addAction(INIT, function () {
    var $masonry = $('.masonry');
    if (!$masonry.length) {
        return;
    }
    $masonry.masonry({
        itemSelector: '.content-column',
        percentPosition: true,
        gutter: 0
    });
});
