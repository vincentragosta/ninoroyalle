<?php

use ChildTheme\BlogPost\BlogPost;
use ChildTheme\Components\PostActionCard\PostActionCardView;
use ChildTheme\Podcast\PodcastEpisodeRepository;
use ChildTheme\Support\PreferredDimensionsHandler;

$BlogPost = BlogPost::createFromGlobal();
$PodcastEpisodeRepository = new PodcastEpisodeRepository();
?>

<div class="content-section">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?php if (($image = $BlogPost->featuredImage()) instanceof \WP_Image): ?>
                        <?= $image->width(PreferredDimensionsHandler::DETAIL_WIDTH)->height(PreferredDimensionsHandler::DETAIL_HEIGHT); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="single-grid">
    <div class="single-grid__container container">
        <div class="single-grid__main">
            <section class="content-section content-section--mb-half">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <div class="content-column col-md-12">
                            <div class="content-column__inner">
                                <?php if ($title = $BlogPost->title()): ?>
                                    <h1 class="heading heading--xlarge"><?= $title; ?></h1>
                                <?php endif; ?>
                                <ul class="list list--inline list--dotted">
                                    <li>Posted on <?= $BlogPost->publishedDate(PostActionCardView::DATE_FORMAT); ?></li>
                                    <li><?= $BlogPost->getCommentsNumber(); ?> Comments</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php if ($content = $BlogPost->content(false)): ?>
                <?= $content; ?>
            <?php endif; ?>
        </div>
        <div class="single-grid__sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
