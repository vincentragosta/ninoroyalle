<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\View\Link;
use ChildTheme\Options\GlobalOptions;
use Backstage\Support\DateTime;

?>

<footer class="footer-nav">
    <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
        <div class="footer-nav__social  text--center container">
            <ul class="list list--inline">
                <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                    <li>
                        <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                            <?= sprintf('%s %s', new IconView(['icon_name' => $SocialIcon->getName()]), $SocialIcon->getLabel()); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="footer-nav__instagram">
        <?= do_shortcode('[instagram-feed]'); ?>
    </div>
    <div class="footer-nav__copyright container">
        <p>
            <?= sprintf(
                '&copy; %s %s | By: %s',
                new DateTime('now', 'Y'),
                'Nino Royalle',
                new Link('https://vincentragosta.io', 'Vincent Ragosta', ['target' => '_blank'])
            ); ?>
        </p>
        <?php if (has_nav_menu('footer_navigation')): ?>
            <?= wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav list--inline']); ?>
        <?php endif; ?>
    </div>
</footer>
<?= ModalView::unloadAll(); ?>
